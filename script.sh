#!/bin/bash



while [ "$1" != "" ]
do
	case $1 in
	help)
		./help.sh
		break
		;;
	tudor)
		./sshd_configuration.sh
		break
		;;
	diana)
		./tools_installation.sh
		break
		;;
	giulia)
		./date_name.sh
		break
		;;
	sabrina)
		./motd_config.sh
		break
		;;
	tibi)
		./update.sh
		break
		;;
	alexseb)
		./user_creation.sh
		break
		;;
	*)
		./help.sh
		break
		;;
esac
done



