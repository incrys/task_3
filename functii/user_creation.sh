#!/bin/sh


add_a_user()
{
input="/git/useri.txt"
while IFS= read -r line
do

USER="$(cut -d';' -f1 <<<"$line")"
COMM="$(cut -d';' -f2 <<<"$line")"
KEY="$(cut -d';' -f3 <<<"$line")"
#su - ${USER} -c "umask 022 ; mkdir .ssh ; echo $SSH_PUBLIC_KEY >> .ssh/authorised_keys"
echo "Adding user $USER ..."
useradd -c "$COMM" $USER
echo "Added user $USER ($COMM)"
echo " "
echo " "
echo " "

mkdir /home/$USER/.ssh
touch /home/$USER/.ssh/id_rsa.pub
echo $KEY > /home/$USER/.ssh/id_rsa.pub
done < "$input"
}

add_a_user
