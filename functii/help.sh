#!/bin/bash

ajutor() {

	clear
	echo "You entered the Help menu"
	echo " "
	echo "The script should be run with the following command: "
	echo " "
	echo "--- ./script.sh -command --- where command can be: "
	echo "               -d   = tools_instalation 'vim' or 'bash' or 'net' "
	echo "               -g   = date_name 'hostname' "
	echo "               -s   = motd_config 'message'  "
	echo "               -u   = system update "update" "
	echo "               -t   = sshd_configuration"
	echo "               -a   = user_creation"
	echo "               -h   = help logic"



}

ajutor
