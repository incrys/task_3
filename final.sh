#!/bin/bash


while getopts "htdg:s:u:a" opt;
do
	case ${opt} in
	h)
		ajutor() {

        clear
        echo "You entered the Help menu"
        echo " "
        echo "The script should be run with the following command: "
        echo " "
        echo "--- ./script.sh -command --- where command can be: "
        echo "               -d   = tools_instalation 'vim' or 'bash' or 'net' "
        echo "               -g   = date_name 'hostname' "
        echo "               -s   = motd_config 'message'  "
        echo "               -u   = system update "update" "
        echo "               -t   = sshd_configuration"
        echo "               -a   = user_creation"
        echo "               -h   = help logic"



	}
	
	ajutor
		;;




	t)
		ssh() {
		##  Modific portul
			sed -i 's/Port 22/Port 58342/' /etc/ssh/sshd_config

			systemctl stop firewalld

		##  Dau disable la Root Login
			sed -i 's/PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config

			systemctl restart sshd

		}
	
		ssh 2> eroriletale2petale/ssh.txt

		if [ $? -eq 0 ]; then
  		 echo "Functia SSH-CONFIGURATION a rulat cu succes"
		else
		 echo "EROARE. Consultati fisierul ssh.txt"
		fi

		;;



	d) 
		tools() {

       		 yum -y install vim bash-completion net-tools 2> eroriletale2petale/tools.sh
               if [ $? -eq 0 ]; then
                 echo "Functia de instalare a rulat cu succes"
                else
                 echo "EROARE. Consultati fisierul tools.txt"
                fi
		}

		;;




	g) arg1=$OPTARG

		date_name() {

		        timedatectl set-timezone Europe/Bucharest
     			   echo "Timezone set to EEST"
#echo "What hostname?"
#read hos
       			 hostnamectl set-hostname "$arg1"
       			 echo "Hostname set to $arg1"

}


		date_name 2> eroriletale2petale/date.txt
		if [ $? -eq 0 ]; then
                 echo "Functia DATE_NAME a rulat cu succes"
                else
                 echo "EROARE. Consultati fisierul date.txt"
                fi

		;;





	s) arg3=$OPTARG
		motd() {

#echo "Welcome $(whoami)!" >> /etc/motd

#echo "What message do you want when log in?"

#read message

			echo " $arg3, $(whoami)!" > /etc/motd
			echo " Your message is $arg3"
		}
		motd 2> eroriletale2petale/motd.txt

		if [ $? -eq 0 ]; then
                 echo "Functia MOTD a rulat cu succes"
                else
                 echo "EROARE. Consultati fisierul motd.txt "
                fi

		;;





	u) answear=$OPTARG
	
		#System_Update()

		update() {

			update="update"
			#echo   Do you want to update the System???
			#read answer
			if [ "$answear" == "$update" ]; then yum -y update
			else
       				 echo Ok!Maybe later!
			fi

			}
		update 2> eroriletale2petale/update.txt
		if [ $? -eq 0 ]; then
                 echo "Functia UPDATE a rulat cu succes"
                else
                 echo "EROARE. Consultati fisierul update.txt"
                fi

		;;




	a)
		

		add_a_user(){

		input="/git/useri.txt"
		while IFS= read -r line
		do

			USER="$(cut -d';' -f1 <<<"$line")"
			COMM="$(cut -d';' -f2 <<<"$line")"
			KEY="$(cut -d';' -f3 <<<"$line")"
			echo "Adding user $USER ..."
			useradd -c "$COMM" $USER
			echo "Added user $USER ($COMM)"
			echo " "
			echo " "
			echo " "
			mkdir /home/$USER/.ssh
			touch /home/$USER/.ssh/id_rsa.pub
			echo "$KEY" > /home/$USER/.ssh/id_rsa.pub
		done < "$input"
		}
		add_a_user 2> aduser.txt

		if [ $? -eq 0 ]; then
                 echo "Functia ADD USER a rulat cu succes"
                else
                 echo "EROARE. Consultati fisierul aduser.txt"
		
                fi


		;;
esac
done


